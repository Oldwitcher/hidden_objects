﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCode : MonoBehaviour
{
    public Rigidbody2D rb;
    public bool inGame;
    public Transform paddleBollPlace;
    public float speed;
    public GMCode gm;
    [SerializeField]
    private Transform brickPunch;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        

    }
    public void Init(GMCode gmA)
    {
        gm = gmA;
    }

    // Update is called once per frame
    void Update()
    {
        if(gm.gameOver)
        {
            return;
        }
        if(!inGame)
        {
            transform.position = paddleBollPlace.position;
        }
        if(Input.GetButtonDown ("Jump") && !inGame)
        {
            inGame = true;
            rb.AddForce(Vector2.up * speed);
        }
    }
    
     void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bottom"))
        {
            Debug.Log("Ball hit Bottom");
            rb.velocity = Vector2.zero;
            inGame = false;
            gm.UpdateLives(-1);
        }
            

    }
     void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.CompareTag("brick"))
        {
            
            Transform newPunch = Instantiate(brickPunch, collision.transform.position, collision.transform.rotation);
            Destroy(newPunch.gameObject, 2.5f);
            Destroy(collision.gameObject);
            gm.UpdateScore(collision.gameObject.GetComponent<BrickCode>().points);
            gm.UpdateNumberOfBricks();
        }
    }
}
