﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleCode : MonoBehaviour
{
    public float speed;
    public float lefttwall;
    public float rightwall;
    public GMCode gm;
    // Start is called before the first frame update
    void Start()
    {

    }
    public void Init(GMCode gmA)
    {
        gm = gmA;
    }


    // Update is called once per frame
    void Update()
    {
        if (gm.gameOver)
        {
            return;
        }
        float horizont = Input.GetAxis("Horizontal");
        transform.Translate(Vector2.right * horizont * Time.deltaTime * speed);
        if (transform.position.x < lefttwall)
        {
            transform.position = new Vector2(lefttwall, transform.position.y);
        }
        if (transform.position.x > rightwall)
        {
            transform.position = new Vector2(rightwall, transform.position.y);
        }

    }
}
